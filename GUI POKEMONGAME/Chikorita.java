
package pokemongame;

public class Chikorita extends Pokemon implements Skillgrass,Skillnormal{
    
    public Chikorita(String name){
        super(name,500,300,1);
    }
    
    public void printattack1(){
        System.out.println("1.Magical Leaf: 20HP , 25MP");
    }

    public void printattack2(){
        System.out.println("2.Solar Beam: 25HP , 50MP");
    }

    public void printattack3(){
        System.out.println("3.Tackle: 15HP , 25MP");
    }

    public void printattack4(){
        System.out.println("4.Growl: 40Hp , 100MP");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        magicalleaf(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        solarbeam(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        tackle(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        growl(myPokemon,enemy);
    }

    public void magicalleaf(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }

    public void solarbeam(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }

    public void tackle(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }

    public void growl(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
        
    }

}
