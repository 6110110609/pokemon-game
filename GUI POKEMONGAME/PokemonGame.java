
package pokemongame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PokemonGame extends JFrame{
    
    public PokemonGame(){
        super("POKEMON GAME");
        Container c = getContentPane();
                
        JLabel background;
        ImageIcon bg = new ImageIcon("background.jpg");
        c.setLayout( new FlowLayout() );
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        
        JLabel play = new JLabel("PLAY GAME");
        play.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        play.setBounds(703,615,120,40);
        background.add(play);
        
        JButton click = new JButton("Click!!!");
        click.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        click.setBounds(700,650,120,40);
        background.add(click);
        click.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               setVisible(false);
               new GuiChooseGender();
           }
        });
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        
    }
    
    public static void main(String[] args) {
        new PokemonGame();
    }
    
}
