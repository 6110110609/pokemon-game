
package pokemongame;

public class Pikachu extends Pokemon implements Skillelectric,Skillfairy{
    public Pikachu(String name){
        super(name,500,300,4);
    }

    public void printattack1(){
        System.out.println("1.Charm: 20HP , 25MP");
    }

    public void printattack2(){
        System.out.println("2.Sweet kiss: 25HP , 50MP");
    }

    public void printattack3(){
        System.out.println("3.Thunder Shock: 15HP , 25MP");
    }

    public void printattack4(){
        System.out.println("4.Thunder Bolt: 40Hp , 100MP");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        charm(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        sweetkiss(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        thundershock(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        thunderbolt(myPokemon,enemy);
    }

    //Fairy
    public void charm(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
    public void sweetkiss(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
    //Electric
    public void thundershock(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }

    public void thunderbolt(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
}
