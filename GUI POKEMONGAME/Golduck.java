
package pokemongame;

public class Golduck extends Pokemon implements Skillwater,Skillnormal{
    public Golduck(String name){
        super(name,500,300,5);
    }

    public void printattack1(){
        System.out.println("1.Water Gun: 20HP , 25MP");
    }

    public void printattack2(){
        System.out.println("2.Hydro Pump: 25HP , 50MP");
    }

    public void printattack3(){
        System.out.println("3.Aqua Tail: 15HP , 25MP");
    }

    public void printattack4(){
        System.out.println("4.Tackle: 40Hp , 100MP");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        watergun(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        hydropump(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        aquatail(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        tackle(myPokemon,enemy);
    }

    //Water
    public void watergun(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
    public void hydropump(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
    
    public void aquatail(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
    //Normal
    public void growl(Pokemon myPokemon,Pokemon enemy){}

    public void tackle(Pokemon myPokemon,Pokemon enemy){
        System.out.println(name + " attack " + enemy.getName());
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
        System.out.println(name + " HP: " + super.getHp() + " MP: " + super.getMp());
        System.out.println(enemy.getName() + " HP: " + enemy.getHp() + " MP: " + enemy.getMp());
    }
}
