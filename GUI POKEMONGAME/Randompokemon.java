
package pokemongame;

import java.util.*;

public class Randompokemon {
    public static ArrayList<Pokemon> getPokemons(int num){
        ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
        if(num < 1){
            return pokemons;
        }
        int pokemonNumber = (int)(Math.random() * num) + 1;
        for(int i = 0 ; i<pokemonNumber ; ++i){
        int type = (int)(Math.random() * 5);
            if(type == 0)
                pokemons.add(new Chikorita("Wild Chikorita"));
            else if(type == 1)
                pokemons.add(new Cyndaquil("Wild Cyndaquil"));
            else if(type == 2)
                pokemons.add(new Totodile("Wild Totodile"));
            else if(type == 3)
                pokemons.add(new Pikachu("Wild Pikachu"));
            else if(type == 4)
                pokemons.add(new Golduck("Wild Goulduck"));
        }
        return pokemons;
    }
}
