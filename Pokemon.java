
public class Pokemon{
    protected String name;
    protected int hp;
    protected int mp;
    protected int rememberhp;
    protected int remembermp;
    protected String spacial;

    public Pokemon(){}

    public Pokemon(String name){
        this.name = name;
        this.hp = 0;
        this.mp = 0;
    }

    public Pokemon(String name , int maxHp , int maxMp){
        this.name = name;
        this.hp = (int)(Math.random() * (maxHp - 50)) + 51;
        this.rememberhp = this.hp;
        this.mp = (int)(Math.random() * (maxMp - 50)) + 51;
        this.remembermp = this.mp;
    }

    public String getName(){
        return this.name;
    }

    public int getHp(){
        return this.hp;
    }

    public int getRememberhp(){
        return this.rememberhp;
    }

    public int getMp(){
        return this.mp;
    }

    public int getRemembermp(){
        return this.remembermp;
    }

    public String getSpacial(){
        return this.spacial;
    }

    public void setHp(int rememberhp){
        this.hp = rememberhp;
    }

    public void setMp(int remembermp){
        this.mp = remembermp;
    }

    public boolean reduce(int mpreduce){
        int currentMp = this.mp - mpreduce;

        if(currentMp >= 0 && currentMp <= this.remembermp){
            this.mp = currentMp;
        }
        else{
            System.out.println("Not enough Mp to attack Pokemon enemy");
            return false;
        }

        return true;
    }

    public boolean damage(int hpdamage){
        if(hp == 0 || mp == 0){
            return false;
        }

        int currentHp = this.hp - hpdamage;
        
        if(currentHp >= 0 && currentHp <= this.rememberhp){
            this.hp = currentHp;
        }
        else{
            this.hp = 0;
        }

        return true;
    }

    public String toString(){
        return this.name;
    }

    public boolean healHp(int hpheal){
        if(this.hp == 0){
            return false;
        }
        
        int currentHp = this.hp + hpheal;
        if(currentHp <= this.rememberhp){
            this.hp = currentHp;
        }
        else{
            this.hp = this.rememberhp;
        }

        return true;
    }

    public boolean healMp(int mpheal){
        if(this.mp < 0){
            return false;
        }

        int currentMp = this.mp + mpheal;
        if(currentMp >= 0 && currentMp <= this.remembermp){
            this.mp = currentMp;
        }
        else{
            this.mp = this.remembermp;
        }

        return true;
    }

    public void printattack1(){}
    public void printattack2(){}
    public void printattack3(){}
    public void printattack4(){}
    public void attack1(Pokemon myPokemon,Pokemon enemy){}
    public void attack2(Pokemon myPokemon,Pokemon enemy){}
    public void attack3(Pokemon myPokemon,Pokemon enemy){}
    public void attack4(Pokemon myPokemon,Pokemon enemy){}    
}