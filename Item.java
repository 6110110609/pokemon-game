
import java.util.*;

import jdk.jfr.BooleanFlag;

public class Item {
    private int coin = 500;
    private int berry = 0;
    private int potion = 0;
    private int ether = 0;
    private int superether = 0;
    private int pokeball = 0;

    public int getCoin(){
        return this.coin;
    }

    public int getBerry(){
        return this.berry;
    }

    public int getPotion(){
        return this.potion;
    }

    public int getEther(){
        return this.ether;
    }

    public int getSuperether(){
        return this.superether;
    }

    public int getPokeball(){
        return this.pokeball;
    }    

    public void setCoin(int coin){
        this.coin = coin;
    }
    
    public void setBerry(int berry){
        this.berry = berry;
    }
    public void setPotion(int potion){
        this.potion = potion;
    }
    public void setEther(int ether){
        this.ether = ether;
    }
    public void setSuperether(int superether){
        this.superether = superether;
    }

    public void setPokeball(int pokeball){
        this.pokeball = pokeball;
    }
    
    public void healberry(Pokemon myPokemon){
        this.berry = this.berry - 1;
        myPokemon.healHp(15);
    }
    public void healpotion(Pokemon myPokemon){
        this.potion = this.potion - 1;
        myPokemon.healHp(25);
    }
    public void healether(Pokemon myPokemon){
        this.ether = this.ether - 1;
        myPokemon.healMp(20);
    }
    public void healsuperether(Pokemon myPokemon){
        this.superether = this.superether - 1;
        myPokemon.healMp(30);
    }
    
    public boolean catchpokemon(ArrayList<Pokemon> bag,Pokemon myPokemon,Pokemon wildPokemon){
        this.pokeball = this.pokeball - 1;
        if(wildPokemon.getHp() < 50){
            int random = (int)(Math.random()*2);
            if(random == 1){
                System.out.println("Catch Sucess");
                bag.add(wildPokemon);
                return true;
            }
            else{
                System.out.println("Catch Fail");
                return false;
            }
            
        }
        else if(wildPokemon.getHp() < 100){
            int random = (int)(Math.random()*3);
            if(random == 1){
                System.out.println("Catch Sucess");
                bag.add(wildPokemon);
                return true;
            }
            else{
                System.out.println("Catch Fail");
                return false;
            }
        }
        else{
            System.out.println("Catch Fail");
            return false;
        }
    }
}