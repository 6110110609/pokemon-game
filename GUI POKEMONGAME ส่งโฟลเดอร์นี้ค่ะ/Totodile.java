
public class Totodile extends Pokemon implements Skillwater,Skillnormal{
    public Totodile(String name){
        super(name,500,400,3,"Water Gun","Hydro Pump","Aqua Tail","Growl");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        watergun(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        hydropump(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        aquatail(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        growl(myPokemon,enemy);
    }

    //Water
    public void watergun(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
    }
    public void hydropump(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
    }
    
    public void aquatail(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
    }
    //Normal
    public void tackle(Pokemon myPokemon,Pokemon enemy){}

    public void growl(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
    }
}
