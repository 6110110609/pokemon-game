
public class Cyndaquil extends Pokemon implements Skillfire,Skillnormal,Skillrock{
    public Cyndaquil(String name){
        super(name,500,400,2,"Burn Up","Inferno","Tackle","Rock Throw");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        burnup(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        inferno(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        tackle(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        rockthrow(myPokemon,enemy);
    }

    //Fire
    public void burnup(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
    }

    public void inferno(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
    }
    //Normal
    public void tackle(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
    }

    public void growl(Pokemon myPokemon,Pokemon enemy){}
    //Rock
    public void rockthrow(Pokemon myPokemon,Pokemon enemy){
       if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
    }

    public void rockslide(Pokemon myPokemon,Pokemon enemy){}


}