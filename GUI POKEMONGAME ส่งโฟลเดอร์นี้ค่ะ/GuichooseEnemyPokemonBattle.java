
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class GuichooseEnemyPokemonBattle extends JFrame{
    
    private Pokemon p;
    private int number;
    private JPanel block[];
    private JLabel namepokemon[];
    private JLabel img[];
    private JPanel detail[];
    private JLabel hp[];
    private JLabel mp[];
    private JButton mypoke[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
    public int i = 0;
    
    public GuichooseEnemyPokemonBattle(Trainer player){
        super("POKEMON GAME");
        ArrayList<Pokemon> pokemons = Randompokemon.getPokemons(8);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1480,850);
        setResizable(false);
        setVisible(true);
             
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        
        JLabel title = new JLabel("Pokemon Around You",JLabel.CENTER );
        title.setFont(new Font("Feast of Flesh BB", Font.BOLD, 100));
        c.add(title,BorderLayout.NORTH);
        
        JPanel main = new JPanel();
        main.setLayout(new GridLayout(2,4));
        
        block = new JPanel[pokemons.size()];
        namepokemon = new JLabel[pokemons.size()];
        img = new JLabel[pokemons.size()];
        detail = new JPanel[pokemons.size()];
        hp = new JLabel[pokemons.size()];
        mp = new JLabel[pokemons.size()];
        for(i = 0; i<pokemons.size(); i++){
            
            this.p = pokemons.get(i);
            this.number = p.getNumber();
            
            block[i] = new JPanel();
            block[i].setLayout(new BorderLayout());
            
            namepokemon[i] = new JLabel("",JLabel.CENTER);
            namepokemon[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            
            String pname = "Name: ";
            pname += p.getName();
            namepokemon[i].setText(pname);
            
            block[i].add(namepokemon[i],BorderLayout.NORTH); 

            ImageIcon chikorita = new ImageIcon("chikorita1.png");
            ImageIcon cyndaquil = new ImageIcon("cyndaquil1.png");
            ImageIcon totodile = new ImageIcon("totodile1.png");
            ImageIcon pikachu = new ImageIcon("pikachu1.png");
            ImageIcon poliwhirl = new ImageIcon("poliwhirl1.png");

            img[i] = new JLabel();
            block[i].add(img[i],BorderLayout.CENTER);

            if(number == 1){
                img[i].setIcon(chikorita);
            }
            else if(number == 2){
                img[i].setIcon(cyndaquil);
            }
            else if(number == 3){
                img[i].setIcon(totodile);
            }
            else if(number == 4){
                img[i].setIcon(pikachu);
            }
            else if(number == 5){
                img[i].setIcon(poliwhirl);
            }

            detail[i] = new JPanel();
            detail[i].setLayout(new GridLayout(2,1,10,0));

            hp[i] = new JLabel("HP: " + p.getHp() + "/" + p.getRememberhp());
            hp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            mp[i] = new JLabel("MP: " + p.getMp() + "/" + p.getRemembermp());
            mp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));

            detail[i].add(hp[i]);
            detail[i].add(mp[i]);

            block[i].add(detail[i],BorderLayout.EAST);
            
            mypoke[i].setText("CHOOSE");
            block[i].add(mypoke[i],BorderLayout.SOUTH);

            main.add(block[i]);
            
        }
        c.add(main,BorderLayout.SOUTH);
        
        mypoke[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(0));
                
            }
        });
        
        mypoke[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(1));
            }
        });
        
        mypoke[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(2));
                
            }
        });
        
        mypoke[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(3));
            }
        });
        
        mypoke[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(4));  
            }
        });
        
        mypoke[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(5));
            }
        });
        
        mypoke[6].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(6)); 
            }
        });
        
        mypoke[7].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiChooseMyPokemonBattle(player,pokemons.get(7));
            }
        });
        
//        pack();
        setLocationRelativeTo(null);
    
    
    }
}
