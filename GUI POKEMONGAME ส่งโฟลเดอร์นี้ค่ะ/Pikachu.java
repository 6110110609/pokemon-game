
public class Pikachu extends Pokemon implements Skillelectric,Skillfairy{
    public Pikachu(String name){
        super(name,500,400,4,"Charm","Sweet Kiss","Thunder Shock","Thunder Bolt");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        charm(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        sweetkiss(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        thundershock(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        thunderbolt(myPokemon,enemy);
    }

    //Fairy
    public void charm(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
    }
    public void sweetkiss(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
    }
    //Electric
    public void thundershock(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
    }

    public void thunderbolt(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
    }
}
