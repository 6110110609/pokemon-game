
public interface Skillfairy {
    void charm(Pokemon myPokemon,Pokemon enemy);
    void sweetkiss(Pokemon myPokemon,Pokemon enemy);    
}
