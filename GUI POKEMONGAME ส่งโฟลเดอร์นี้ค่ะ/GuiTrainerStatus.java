
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class GuiTrainerStatus extends JFrame{
    
    private Pokemon p;
    private int number;
    private JPanel block[];
    private JLabel namepokemon[];
    private JLabel img[];
    private JPanel detail[];
    private JLabel hp[];
    private JLabel mp[];
    private JButton rename[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
    private ArrayList<Pokemon> bag;
    public int i = 0;
    
   public GuiTrainerStatus(ArrayList<Pokemon> bag){
       super("POKEMON GAME");
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       setSize(1295,685);

        setResizable(false);
        setVisible(true);
        
        this.bag = bag;
             
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        
        JPanel main = new JPanel();
        main.setLayout(new GridLayout(2,4));
        
        block = new JPanel[bag.size()];
        namepokemon = new JLabel[bag.size()];
        img = new JLabel[bag.size()];
        detail = new JPanel[bag.size()];
        hp = new JLabel[bag.size()];
        mp = new JLabel[bag.size()];
        for(i = 0; i<bag.size(); i++){
            
            this.p = bag.get(i);
            this.number = p.getNumber();
            
            block[i] = new JPanel();
            block[i].setLayout(new BorderLayout());
            
            namepokemon[i] = new JLabel("",JLabel.CENTER);
            namepokemon[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            
            String pname = "Name: ";
            pname += p.getName();
            namepokemon[i].setText(pname);
            
            block[i].add(namepokemon[i],BorderLayout.NORTH); 

            ImageIcon chikorita = new ImageIcon("chikorita1.png");
            ImageIcon cyndaquil = new ImageIcon("cyndaquil1.png");
            ImageIcon totodile = new ImageIcon("totodile1.png");
            ImageIcon pikachu = new ImageIcon("pikachu1.png");
            ImageIcon poliwhirl = new ImageIcon("poliwhirl1.png");

            img[i] = new JLabel();
            block[i].add(img[i],BorderLayout.CENTER);

            if(number == 1){
                img[i].setIcon(chikorita);
            }
            else if(number == 2){
                img[i].setIcon(cyndaquil);
            }
            else if(number == 3){
                img[i].setIcon(totodile);
            }
            else if(number == 4){
                img[i].setIcon(pikachu);
            }
            else if(number == 5){
                img[i].setIcon(poliwhirl);
            }

            detail[i] = new JPanel();
            detail[i].setLayout(new GridLayout(2,1,10,0));

            hp[i] = new JLabel("HP: " + p.getHp() + "/" + p.getRememberhp());
            hp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            mp[i] = new JLabel("MP: " + p.getMp() + "/" + p.getRemembermp());
            mp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));

            detail[i].add(hp[i]);
            detail[i].add(mp[i]);

            block[i].add(detail[i],BorderLayout.EAST);
            
            rename[i].setText("RENAME");
            block[i].add(rename[i],BorderLayout.SOUTH);

            main.add(block[i]);    
        }
        c.add(main);
        
        rename[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(0).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(1).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(2).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(3).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(4).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(5).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[6].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(6).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        
        rename[7].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String name = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "RENAME", JOptionPane.PLAIN_MESSAGE);
                if(name != null){
                    bag.get(7).rename(name);
                    setVisible(false);
                    new GuiTrainerStatus(bag);
                }
            }
        });
        // pack();
        setLocationRelativeTo(null);
   }
}
