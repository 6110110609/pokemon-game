
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class GuiChooseMyPokemonBattle extends JFrame{
    
    private Pokemon p;
    private int number;
    private JPanel block[];
    private JLabel namepokemon[];
    private JLabel img[];
    private JPanel detail[];
    private JLabel hp[];
    private JLabel mp[];
    private JButton mypoke[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
    public int i = 0;
    
   public GuiChooseMyPokemonBattle(Trainer player,Pokemon enemyPokemon){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1480,850);
        setResizable(false);
        setVisible(true);
          
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        
        JLabel title = new JLabel("Select My Pokemon",JLabel.CENTER );
        title.setFont(new Font("Feast of Flesh BB", Font.BOLD, 100));
        c.add(title,BorderLayout.NORTH);
        
        JPanel main = new JPanel();
        main.setLayout(new GridLayout(2,4));
        
        block = new JPanel[player.getBag().size()];
        namepokemon = new JLabel[player.getBag().size()];
        img = new JLabel[player.getBag().size()];
        detail = new JPanel[player.getBag().size()];
        hp = new JLabel[player.getBag().size()];
        mp = new JLabel[player.getBag().size()];
        for(i = 0; i<player.getBag().size(); i++){
            
            this.p = player.getBag().get(i);
            this.number = p.getNumber();
            
            block[i] = new JPanel();
            block[i].setLayout(new BorderLayout());
            
            namepokemon[i] = new JLabel("",JLabel.CENTER);
            namepokemon[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            
            String pname = "Name: ";
            pname += p.getName();
            namepokemon[i].setText(pname);
            
            block[i].add(namepokemon[i],BorderLayout.NORTH); 

            ImageIcon chikorita = new ImageIcon("chikorita1.png");
            ImageIcon cyndaquil = new ImageIcon("cyndaquil1.png");
            ImageIcon totodile = new ImageIcon("totodile1.png");
            ImageIcon pikachu = new ImageIcon("pikachu1.png");
            ImageIcon poliwhirl = new ImageIcon("poliwhirl1.png");

            img[i] = new JLabel();
            block[i].add(img[i],BorderLayout.CENTER);

            if(number == 1){
                img[i].setIcon(chikorita);
            }
            else if(number == 2){
                img[i].setIcon(cyndaquil);
            }
            else if(number == 3){
                img[i].setIcon(totodile);
            }
            else if(number == 4){
                img[i].setIcon(pikachu);
            }
            else if(number == 5){
                img[i].setIcon(poliwhirl);
            }

            detail[i] = new JPanel();
            detail[i].setLayout(new GridLayout(2,1,10,0));

            hp[i] = new JLabel("HP: " + p.getHp() + "/" + p.getRememberhp());
            hp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
            mp[i] = new JLabel("MP: " + p.getMp() + "/" + p.getRemembermp());
            mp[i].setFont(new Font("Eras Demi ITC", Font.BOLD, 20));

            detail[i].add(hp[i]);
            detail[i].add(mp[i]);

            block[i].add(detail[i],BorderLayout.EAST);
            
            mypoke[i].setText("CHOOSE");
            block[i].add(mypoke[i],BorderLayout.SOUTH);

            main.add(block[i]);
            
        }
        c.add(main);
        
        mypoke[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(0),enemyPokemon);
                
            }
        });
        
        mypoke[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(1),enemyPokemon);
            }
        });
        
        mypoke[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(2),enemyPokemon);
                
            }
        });
        
        mypoke[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(3),enemyPokemon);
            }
        });
        
        mypoke[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(4),enemyPokemon);  
            }
        });
        
        mypoke[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(5),enemyPokemon);
            }
        });
        
        mypoke[6].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(6),enemyPokemon); 
            }
        });
        
        mypoke[7].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattle(player,player.getBag().get(7),enemyPokemon);
            }
        });
        
//        pack();
        setLocationRelativeTo(null);
    
   }
}
