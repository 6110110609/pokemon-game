
import java.util.*;

public class Choice {

    public boolean buy(Item item,int no){

            if(no == 1){
                if(item.getCoin() >= 25){
                    item.setCoin(item.getCoin()-25);
                    item.setBerry(item.getBerry()+1);
                    return true;
                }
            }
            else if(no == 2){
                if(item.getCoin() >= 45){
                    item.setCoin(item.getCoin()-45);
                    item.setPotion(item.getPotion()+1);
                    return true;
                }
            }
            else if(no == 3){
                if(item.getCoin() >= 35){
                    item.setCoin(item.getCoin()-35);
                    item.setEther(item.getEther()+1);
                    return true;
                }
            }
            else if(no == 4){
                if(item.getCoin() >= 45){
                    item.setCoin(item.getCoin()-50);
                    item.setSuperether(item.getSuperether()+1);
                    return true;
                }
            }
            else if(no == 5){
                if(item.getCoin() >= 100){
                    item.setCoin(item.getCoin()-100);
                    item.setPokeball(item.getPokeball()+1);
                    return true;
                }
            }
        return false;
    }

    public void feed(AbstractList<Pokemon> bag){
        for(Pokemon p : bag){
            p.setHp(p.getRememberhp());
            p.setMp(p.getRemembermp());
        }
    }
}
