
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GuiRename {
    
    public GuiRename(Pokemon p){
        String namepoke = JOptionPane.showInputDialog(null, "Rename of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
        if(namepoke != null){
            p.rename(namepoke);
        }
    }
}
