
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class GuiTrainer extends JFrame{
    private Trainer player;
    private ArrayList<Pokemon> bag;
    private int number = 1;
    
    public GuiTrainer(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(1450,850);
        setResizable(false);
        setVisible(true);
        
        this.player = player;
        this.bag = player.getBag();
        
        Container c = getContentPane();
        
        c.setLayout(new BorderLayout());

        JPanel head = new JPanel();
        head.setLayout(new GridLayout(2,1,0,30));
        
        ImageIcon choose1 = new ImageIcon("choose1.png");
        ImageIcon choose2 = new ImageIcon("choose2.png");
        ImageIcon choose3 = new ImageIcon("choose3.png");
        ImageIcon choose4 = new ImageIcon("choose4.png");
        ImageIcon choose5 = new ImageIcon("choose5.png");
        
        JPanel frame = new JPanel();
//        frame.setLayout(null);
        frame.setBackground(Color.white);
        
        JLabel title = new JLabel(choose1);
        
        frame.add(title);
        head.add(frame);
        
        JPanel skill = new JPanel();
        skill.setLayout(new GridLayout(1,4));
           
        ImageIcon s1 = new ImageIcon("skillchi1.png");
        ImageIcon s2 = new ImageIcon("skillchi2.png");
        ImageIcon s3 = new ImageIcon("skillchi3.png");
        ImageIcon s4 = new ImageIcon("skillchi4.png");
        ImageIcon s5 = new ImageIcon("skillcyn1.png");
        ImageIcon s6 = new ImageIcon("skillcyn2.png");
        ImageIcon s7 = new ImageIcon("skillcyn3.png");
        ImageIcon s8 = new ImageIcon("skillcyn4.png");
        ImageIcon s9 = new ImageIcon("skilltoto1.png");
        ImageIcon s10 = new ImageIcon("skilltoto2.png");
        ImageIcon s11 = new ImageIcon("skilltoto3.png");
        ImageIcon s12 = new ImageIcon("skilltoto4.png");
        ImageIcon s13 = new ImageIcon("skillpika1.png");
        ImageIcon s14 = new ImageIcon("skillpika2.png");
        ImageIcon s15 = new ImageIcon("skillpika3.png");
        ImageIcon s16 = new ImageIcon("skillpika4.png");
        ImageIcon s17 = new ImageIcon("skillpoli1.png");
        ImageIcon s18 = new ImageIcon("skillpoli2.png");
        ImageIcon s19 = new ImageIcon("skillpoli3.png");
        ImageIcon s20 = new ImageIcon("skillpoli4.png");

        JLabel skill1 = new JLabel(s1);
        JLabel skill2 = new JLabel(s2);
        JLabel skill3 = new JLabel(s3);
        JLabel skill4 = new JLabel(s4);
        
        skill.setBackground(Color.white);
        
        skill.add(skill1);
        skill.add(skill2);
        skill.add(skill3);
        skill.add(skill4);
        
        head.add(skill);
        
        c.add(head,BorderLayout.NORTH);
        
        JPanel choose = new JPanel();
        choose.setLayout(new GridLayout(2,1));
        
        JPanel pokemon = new JPanel();
        pokemon.setLayout(new FlowLayout());
        
        JButton poke1 = new JButton("Chicolita");
        JButton poke2 = new JButton("Cyndaquil");
        JButton poke3 = new JButton("Totodile");
        JButton poke4 = new JButton("Pikachu");
        JButton poke5 = new JButton("Poliwhirl");
        
        poke1.setFont(new Font("SNES Italic", Font.BOLD, 30));
        poke2.setFont(new Font("SNES Italic", Font.BOLD, 30));
        poke3.setFont(new Font("SNES Italic", Font.BOLD, 30));
        poke4.setFont(new Font("SNES Italic", Font.BOLD, 30));
        poke5.setFont(new Font("SNES Italic", Font.BOLD, 30));

        pokemon.add(poke1);
        pokemon.add(poke2);
        pokemon.add(poke3);
        pokemon.add(poke4);
        pokemon.add(poke5);
        
        JButton ok = new JButton("OK");
        ok.setFont(new Font("SNES Italic", Font.BOLD, 30));
        
        choose.add(pokemon);
        choose.add(ok);
        
        c.add(choose,BorderLayout.SOUTH);
        
        ImageIcon chikorita = new ImageIcon("chikorita.png");
        ImageIcon cyndaquil = new ImageIcon("cyndaquil.png");
        ImageIcon totodile = new ImageIcon("totodile.png");
        ImageIcon pikachu = new ImageIcon("pikachu.png");
        ImageIcon poliwhirl = new ImageIcon("poliwhirl.png");
        
        JLabel img;
        img = new JLabel(chikorita);
        c.add(img,BorderLayout.CENTER);
        
        poke1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                title.setIcon(choose1);
                skill1.setIcon(s1);
                skill2.setIcon(s2);
                skill3.setIcon(s3);
                skill4.setIcon(s4);
                img.setIcon(chikorita);
                number = 1;
            }
        });
        
        poke2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                title.setIcon(choose2);
                skill1.setIcon(s5);
                skill2.setIcon(s6);
                skill3.setIcon(s7);
                skill4.setIcon(s8);
                img.setIcon(cyndaquil);
                number = 2;
            }
        });
        
        poke3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                title.setIcon(choose3);
                skill1.setIcon(s9);
                skill2.setIcon(s10);
                skill3.setIcon(s11);
                skill4.setIcon(s12);
                img.setIcon(totodile);
                number = 3;
            }
        });
        
        poke4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                title.setIcon(choose4);
                skill1.setIcon(s13);
                skill2.setIcon(s14);
                skill3.setIcon(s15);
                skill4.setIcon(s16);
                img.setIcon(pikachu);
                number = 4;
            }
        });
        
        poke5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                title.setIcon(choose5);
                skill1.setIcon(s17);
                skill2.setIcon(s18);
                skill3.setIcon(s19);
                skill4.setIcon(s20);
                img.setIcon(poliwhirl);
                number = 5;
            }
        });
        
        ok.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(number == 1){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Chikorita(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 2){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Cyndaquil(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 3){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Totodile(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 4){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Pikachu(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 5){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Poliwhirl(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }                    
                }
        
            }
        });
        pack();
        setLocationRelativeTo(null);
    }
}
