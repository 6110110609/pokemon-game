
import java.util.*;

public class Trainer {
    private ArrayList<Pokemon> bag;
    private Item item = new Item();
    private String name;

    public Trainer(String name){
        bag = new ArrayList<Pokemon>();
        this.name = name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setBag(ArrayList<Pokemon> bag){
        this.bag = bag;
    }
    
    public ArrayList<Pokemon> getBag(){
        return bag;
    }
    
    public void setItem(Item item){
        this.item = item;
    }
    
    public Item getItem(){
        return item;
    }
}
