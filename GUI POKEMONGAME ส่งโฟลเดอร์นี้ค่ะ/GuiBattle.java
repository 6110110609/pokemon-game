
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class GuiBattle extends JFrame{
    public GuiBattle(Trainer player,Pokemon myPokemon,Pokemon enemyPokemon){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(1480,850);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();
        
        JLabel background;
        ImageIcon bg = new ImageIcon("battleframe.png");
        background = new JLabel(bg);
        c.add(background);
        
        ImageIcon mypoke1 = new ImageIcon("chikorita2.png");
        ImageIcon mypoke2 = new ImageIcon("cyndaquil2.png");
        ImageIcon mypoke3 = new ImageIcon("totodile2.png");
        ImageIcon mypoke4 = new ImageIcon("pikachu2.png");
        ImageIcon mypoke5 = new ImageIcon("poliwhirl2.png");
        ImageIcon mypoke6 = new ImageIcon("chikorita3.png");
        ImageIcon mypoke7 = new ImageIcon("cyndaquil3.png");
        ImageIcon mypoke8 = new ImageIcon("totodile3.png");
        ImageIcon mypoke9 = new ImageIcon("pikachu3.png");
        ImageIcon mypoke10 = new ImageIcon("poliwhirl3.png");
           
        JLabel mypoke = new JLabel("");
        JLabel enemepoke = new JLabel("");
        
        if(myPokemon.getNumber() == 1){
            mypoke.setIcon(mypoke1);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 2){
            mypoke.setIcon(mypoke2);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 3){
            mypoke.setIcon(mypoke3);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 4){
            mypoke.setIcon(mypoke4);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 5){
            mypoke.setIcon(mypoke5);
            mypoke.setBounds(0,0,1480,850);
        }
        
        if(enemyPokemon.getNumber() == 1){
            enemepoke.setIcon(mypoke6);
            enemepoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 2){
            enemepoke.setIcon(mypoke7);
            enemepoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 3){
            enemepoke.setIcon(mypoke8);
            enemepoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 4){
            enemepoke.setIcon(mypoke9);
            enemepoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 5){
            enemepoke.setIcon(mypoke10);
            enemepoke.setBounds(0,0,1480,850);
        }

        JButton click = new JButton("c");
        click.setBounds(740, 425, 0, 0);
        
        JLabel text = new JLabel("Press Spacebar...");
        text.setFont(new Font("Feast of Flesh BB", Font.BOLD, 50));
        text.setBounds(575, 600, 500, 70);
        
        background.add(text);
        background.add(click);
        background.add(mypoke);
        background.add(enemepoke);
        
        click.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               setVisible(false);
               new GuiBattleGround(player,myPokemon,enemyPokemon,myPokemon.getNumber());
           }
        });
        
        pack();
        setLocationRelativeTo(null);     
    }
}
