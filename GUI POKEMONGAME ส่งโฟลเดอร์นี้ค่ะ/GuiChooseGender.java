
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GuiChooseGender extends JFrame{
    private Trainer player;
    
    public  GuiChooseGender(){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1450,850);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();
        
        JLabel background;
        ImageIcon bg = new ImageIcon("bgtrainer.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        
        JLabel title = new JLabel("CHOOSE TRAINER");
        title.setFont(new Font("Feast of Flesh BB", Font.BOLD, 80));
        title.setBounds(470,20,800,100);
        background.add(title);
        
        JButton boy = new JButton("BOY");
        boy.setBounds(525,725,120,40);
        
        JButton girl = new JButton("Girl");
        girl.setBounds(850,725,120,40);
        background.add(boy);
        background.add(girl);
        
        boy.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               player = new Trainer("Lucus");
               setVisible(false);
               new GuiTrainer(player);
           }
        });
        
        girl.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               player = new Trainer("Draw");
               setVisible(false);
               new GuiTrainer(player);
           }
        });
        
    }
}
