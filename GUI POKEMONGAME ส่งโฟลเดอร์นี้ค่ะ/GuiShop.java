
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//import java.util.*;

public class GuiShop extends JFrame{

    private Item item;
    private boolean checkbuy = true;
    
    public GuiShop(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(1480,850);
        setResizable(false);
        setVisible(true);
        
        Choice choice = new Choice();
             
        this.item = player.getItem();       
        
        Container c = getContentPane();

        ImageIcon bg = new ImageIcon("bgshop.png");

        JLabel background = new JLabel(bg);

        c.add(background);

        ImageIcon coin = new ImageIcon("coin.png");
        
        
        JButton buyberry = new JButton("Berry : 25 coins");
        JButton buypotion = new JButton("Potion :45 coins");
        JButton buyether = new JButton("Ether : 35 coins");
        JButton buysuperether = new JButton("Super Ether : 50 coins");
        JButton buypokeball = new JButton("Pokeball : 100 coins");
        
        buyberry.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        buypotion.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        buyether.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        buysuperether.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        buypokeball.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        
        buyberry.setBounds(115, 595, 180, 30);
        buypotion.setBounds(375, 595, 190, 30);
        buyether.setBounds(655, 595, 180, 30);
        buysuperether.setBounds(885, 595, 240, 30);
        buypokeball.setBounds(1155, 595, 220, 30);

        background.add(buyberry);
        background.add(buypotion);
        background.add(buyether);
        background.add(buysuperether);
        background.add(buypokeball);
                
        JLabel namebuy1 = new JLabel("Berry : "+ item.getBerry(),JLabel.CENTER);
        JLabel namebuy2 = new JLabel("Potion : "+ item.getPotion(),JLabel.CENTER);
        JLabel namebuy3 = new JLabel("Ether : "+ item.getEther(),JLabel.CENTER);
        JLabel namebuy4 = new JLabel("Superether : "+ item.getSuperether(),JLabel.CENTER);
        JLabel namebuy5 = new JLabel("Pokeball : " + item.getPokeball(),JLabel.CENTER);


        JLabel mycoin = new JLabel("Coin : " + item.getCoin(),JLabel.CENTER);
        
        namebuy1.setFont(new Font("SNES Italic", Font.BOLD, 35));
        namebuy2.setFont(new Font("SNES Italic", Font.BOLD, 35));
        namebuy3.setFont(new Font("SNES Italic", Font.BOLD, 35));
        namebuy4.setFont(new Font("SNES Italic", Font.BOLD, 35));
        namebuy5.setFont(new Font("SNES Italic", Font.BOLD, 35));
        mycoin.setFont(new Font("SNES Italic", Font.BOLD, 35));

        mycoin.setIcon(coin);
        
        namebuy1.setBounds(115, 650, 180, 40);
        namebuy2.setBounds(385, 650, 180, 40);
        namebuy3.setBounds(655, 650, 180, 40);
        namebuy4.setBounds(885, 650, 240, 40);
        namebuy5.setBounds(1155, 650, 220, 40);

        mycoin.setBounds(620, 720, 210, 60);

        background.add(namebuy1);
        background.add(namebuy2);
        background.add(namebuy3);
        background.add(namebuy4);
        background.add(namebuy5);
        background.add(mycoin);
        
        JButton back = new JButton("BACK");

        back.setBounds(1250, 800, 180, 30);

        background.add(back);
        
        buyberry.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkbuy = choice.buy(item,1);
                if(checkbuy){
                    namebuy1.setText("Berry : "+ item.getBerry());
                    mycoin .setText("Coin : " + item.getCoin());
                    player.setItem(item);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Not enough coin","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        buypotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkbuy = choice.buy(item,2);
                if(checkbuy){
                    namebuy2.setText("Potion : "+ item.getPotion());
                    mycoin .setText("Coin : " + item.getCoin());
                    player.setItem(item);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Not enough coin","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        buyether.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkbuy = choice.buy(item,3);
                if(checkbuy){
                    namebuy3.setText("Ether : "+ item.getEther());
                    mycoin .setText("Coin : " + item.getCoin());
                    player.setItem(item);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Not enough coin","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        buysuperether.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkbuy = choice.buy(item,4);
                if(checkbuy){
                    namebuy4.setText("Super Ether : "+ item.getSuperether());
                    mycoin .setText("Coin : " + item.getCoin());
                    player.setItem(item);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Not enough coin","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        buypokeball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                checkbuy = choice.buy(item,5);
                if(checkbuy){
                    namebuy5.setText("Pokeball : "+ item.getPokeball());
                    mycoin .setText("Coin : " + item.getCoin());
                    player.setItem(item);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Not enough coin","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiPlay(player);
            }
        });
        
        pack();
        setLocationRelativeTo(null);
        
    }
    
}
