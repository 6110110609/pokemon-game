
public interface Skillwater {
    void watergun(Pokemon myPokemon,Pokemon enemy);
    void hydropump(Pokemon myPokemon,Pokemon enemy);
    void aquatail(Pokemon myPokemon,Pokemon enemy);
}