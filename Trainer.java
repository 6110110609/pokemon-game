
import java.util.*;

public class Trainer {
    private ArrayList<Pokemon> bag;
    private Scanner sc;
    Item item = new Item();
    
    public Trainer(){
        boolean next = true;
        
        do{
        bag = new ArrayList<Pokemon>();
        sc = new Scanner(System.in);
        System.out.println("Choose your pokemon");
        System.out.println("1 : Chicolita");
        System.out.println("2 : Cyndaquil");
        System.out.println("3 : Totodile");
        System.out.println("4 : Pikachu");
        System.out.println("5 : Golduck");
        System.out.print("Enter number : ");
        int number = sc.nextInt();
        next = choose(number);
        }while(next);

    }

    public boolean choose(int number){
        String namemypokemon = "";
        sc = new Scanner(System.in);
        
        // System.out.println("");
        if(number == 1){
            System.out.print("\nEnter my pokemon name : ");
            namemypokemon = sc.nextLine();
            bag.add(new Chikorita(namemypokemon));
            return false;
        }
        else if(number == 2){
            System.out.print("\nEnter my pokemon name : ");
            namemypokemon = sc.nextLine();
            bag.add(new Cyndaquil(namemypokemon));
            return false;
        }
        else if(number == 3){
            System.out.print("\nEnter my pokemon name : ");
            namemypokemon = sc.nextLine();
            bag.add(new Totodile(namemypokemon));
            return false;
        }
        else if(number == 4){
            System.out.print("\nEnter my pokemon name : ");
            namemypokemon = sc.nextLine();
            bag.add(new Pikachu(namemypokemon));
            return false;
        }
        else if(number == 5){
            System.out.print("\nEnter my pokemon name : ");
            namemypokemon = sc.nextLine();
            bag.add(new Golduck(namemypokemon));
            return false;
        }
        else{
            System.out.print("\nDon't have number choose pokemon again");
            return true;
        }
    }

    public void play(){
        // System.out.println("Debug");
        Choice choice = new Choice();
        Battlepokemon battlepokemon = new Battlepokemon();
        String cmd = "";
        do{
            System.out.println("\n1.Pokedex");
            System.out.println("2.Battle");
            System.out.println("3.Buy Item");
            System.out.println("4.Pokemon center[feed]");
            System.out.println("5.Quit");
            System.out.print("Enter cmd: ");
            cmd = sc.nextLine();
            System.out.println("");
            if(cmd.equals("1")){
                System.out.println("Status of my pokemon");
                choice.trainerstatus(bag,item);
            }
            else if(cmd.equals("2")){
                System.out.println("Battle pokemon");
                battlepokemon.battle(bag,item);
            }
            else if(cmd.equals("3")){
                System.out.println("Buy Item");
                choice.buy(item);
            }
            else if(cmd.equals("4")){
                System.out.println("POKEMON CENTER [FEED]");
                choice.feed(bag);
            }
        }while(true);
    }

}